import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: "org.hamstudy.runnertracker",
  appName: "TRacer",
  bundledWebRuntime: false,
  webDir: "dist",
  plugins: {
    SplashScreen: {
      launchAutoHide: false,
    },
  },
};
export default config;
