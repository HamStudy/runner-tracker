import config from './playwright.config';
config.projects = config.projects!.filter(p => p.name !== 'unit');
export default config;
