import { expect, test } from '@playwright/test';
import { devices } from 'playwright';
import { PageEventsList } from './PageObjects/pomPageEventsList';
import { PageStationsList } from './PageObjects/pomPageStationsList';
import { PomNumpad } from './PageObjects/pomNumpad';


test('Test auto time in', async ({ page }) => {
  const eventListPage = new PageEventsList(page);
  const stationListPage = new PageStationsList(page);
  const numpad = new PomNumpad(page);
  await page.setViewportSize(devices['iPhone 14 Pro'].viewport);

  await page.goto('/');
  await page.waitForLoadState('domcontentloaded');
  await eventListPage.addEvent('E2E Race');
  await stationListPage.addStation('Station 1');
  await numpad.click1();
  await numpad.click2();
  await numpad.clickAutoTimeIn();
  await page.waitForSelector('text=Participant #12');
  await expect(await page.locator('text=Participant #12').isVisible()).toBeTruthy();
  await expect(await page.locator('.v-list-item__subtitle div:has-text("Time in:") > span').textContent()).not.toBe('—');
  await expect(await page.locator('.v-list-item__subtitle div:has-text("Time out:") > span').textContent()).toBe('—');
});
