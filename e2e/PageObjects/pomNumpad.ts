import { type Page } from '@playwright/test';

export class PomNumpad {
  constructor(private page: Page) {}
  click0() {
    return this.page.click('.keypad .btn-0');
  }
  click1() {
    return this.page.click('.keypad .btn-1');
  }
  click2() {
    return this.page.click('.keypad .btn-2');
  }
  click3() {
    return this.page.click('.keypad .btn-3');
  }
  click4() {
    return this.page.click('.keypad .btn-4');
  }
  click5() {
    return this.page.click('.keypad .btn-5');
  }
  click6() {
    return this.page.click('.keypad .btn-6');
  }
  click7() {
    return this.page.click('.keypad .btn-7');
  }
  click8() {
    return this.page.click('.keypad .btn-8');
  }
  click9() {
    return this.page.click('.keypad .btn-9');
  }
  clickBackspace() {
    return this.page.click('.keypad .btn-backspace');
  }
  clickRepeatLast() {
    return this.page.click('.keypad .btn-repeat');
  }
  clickDnf() {
    return this.page.click('.keypad .btn-dnf');
  }
  clickAutoTimeIn() {
    return this.page.click('.keypad .btn-time-in');
  }
  clickAutoTimeOut() {
    return this.page.click('.keypad .btn-time-out');
  }
  clickNow() {
    return this.page.click('.keypad .btn-now');
  }
  clickNext() {
    return this.page.click('.keypad .btn-next');
  }
  clickCancel() {
    return this.page.click('.keypad .btn-cancel');
  }
}
