import { type Page } from '@playwright/test';

export class PageEventsList {
  constructor(private page: Page) {}

  async addEvent(eventName: string) {
    await this.page.click('.add-btn');
    const eventNameLabel = await this.page.locator('text=Event name*');
    const eventNameInput = eventNameLabel.locator('xpath=following-sibling::input');
    await eventNameInput.fill(eventName);
    await this.page.click('text=Save');
  }
}
