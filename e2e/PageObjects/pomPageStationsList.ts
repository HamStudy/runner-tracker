import { type Page } from '@playwright/test';

export class PageStationsList {
  constructor(private page: Page) {}

  async addStation(stationName = '') {
    await this.page.waitForSelector('.title-bar span:text("Station")');
    if (!this.page.locator('text=Station name*').isVisible()) {
      await this.page.click('.add-btn');
    }
    const stationNameLabel = await this.page.locator('text=Station name*');
    const stationNameInput = stationNameLabel.locator('xpath=following-sibling::input');
    await stationNameInput.fill(stationName);
    await this.page.click('text=Save');
  }
}
