const { AsyncLocalStorage } = require('node:async_hooks');
const { rm, symlink } = require('node:fs/promises');
const { resolve } = require('node:path');
const { exec } = require('node:child_process');
const { chromium, devices } = require('playwright');
const process = require('node:process');

const DATA_DIR = 'app-screenshots/data';
const OUTPUT_DIR = 'app-screenshots/output';
const BASE_URL = 'http://localhost:9000';
const storage = new AsyncLocalStorage();

const screenshotDevices = [{
  name: '6.7" phone (iPhone 14 Pro Max)',
  color: '[36m', // cyan
  info: devices['iPhone 14 Pro Max'],
  safeArea: { top: 59, bottom: 34, left: 0, right: 0 },
}, {
  name: '5.5" phone (iPhone 8 Plus)',
  color: '[33m', // yellow
  info: devices['iPhone 8 Plus'],
  safeArea: { top: 19, bottom: 0, left: 0, right: 0 },
}, {
  name: 'tablet (iPad Pro 11 landscape)',
  color: '[35m', // magenta
  info: devices['iPad Pro 11 landscape'],
  safeArea: { top: 0, bottom: 0, left: 0, right: 0 },
}];

async function runTests() {
  try {
    await rm(OUTPUT_DIR, { recursive: true, force: true });
    await rm('dist/pw-test', { recursive: true, force: true });
    await symlink(resolve(DATA_DIR), 'dist/pw-test', 'dir');
    const browser = await chromium.launch({
      headless: false,
      args: ['--use-fake-device-for-media-stream']
    });
    await Promise.all(screenshotDevices.map(device => storage.run({ browser, device }, generateScreenshots)));
    await browser.close();
  } finally {
    await rm('dist/pw-test', { recursive: true, force: true });
  }
}

async function generateScreenshots() {
  const store = storage.getStore();
  /** @type { import('playwright').Page } */
  const page = await store.browser.newPage({
    ...store.device.info,
    permissions: ['microphone'],
    viewport: store.device.info.screen || store.device.info.viewport, // use entire screen size, if defined
  });
  page.context().addCookies([{
    domain: 'localhost',
    name: 'pw-test',
    path: '/',
    value: 'true',
  }]);
  store.page = page;

  await page.goto(BASE_URL);
  await page.evaluate((safeArea) => {
    document.documentElement.style.setProperty('--override-safe-area-top', `${safeArea.top}px`);
    document.documentElement.style.setProperty('--override-safe-area-bottom', `${safeArea.bottom}px`);
    document.documentElement.style.setProperty('--override-safe-area-left', `${safeArea.left}px`);
    document.documentElement.style.setProperty('--override-safe-area-right', `${safeArea.right}px`);
  }, store.device.safeArea);

  await page.waitForURL(`${BASE_URL}/events`);

  if (await page.locator('#breakpointBanner').count() > 0) {
    throw new Error('Cannot take screenshots in development mode!');
  }

  try {
    await eventList();
    await page.click('text=Snow Peak');
    await stationList();
    await page.click('text=Windy Pass');
    await inputPage();
    await recordsPage();
    await sendPage();
    await overviewPage();
    await receivePage();
  } catch(e) {
    await takeScreenshot('error');
    throw e;
  }
}

async function loadData() {
  const dataFile = 'app-screenshots/data/event.json';
  const { page } = storage.getStore();
  await page.setInputFiles('input[type="file"]', dataFile);
  await page.waitForSelector('.v-snack__content:has-text("Data imported")');
  // click close button
  await page.click('.v-snack__content:has-text("close")');
  // wait for the snackbar to disappear
  await page.waitForSelector('.v-snack__content:has-text("Data imported")', { state: 'hidden' });
}

async function eventList() {
  await loadData();
  await takeScreenshot('event-list');
}

async function stationList() {
  await takeScreenshot('station-list');
}

async function inputPage() {
  const { page } = storage.getStore();
  await clickTabbar('Input');
  await page.keyboard.press('2');
  await page.keyboard.press('2');
  await page.waitForSelector('text=Kaladin Stormblessed');
  await takeScreenshot('01 - Input Page');
}

async function recordsPage() {
  const { page, device } = storage.getStore();
  await clickTabbar('Records');
  await page.click('text=Records');
  await page.waitForSelector('.v-data-table');
  // if device is tablet landscape then click the gear icon in the title bar and toggle split screen
  if (device.name.includes('landscape')) {
    await clickSettings('.v-menu__content .v-input--switch');
  }
  await takeScreenshot('02 - Records Page');
}

async function sendPage() {
  const { page } = storage.getStore();
  await clickTabbar('Send');
  await takeScreenshot('03 - Send Page');
}

async function receivePage() {
  const { page } = storage.getStore();
  await clickTabbar('Receive');
  await page.click('text="Start listening"');
  await page.waitForSelector('text="Listening for input..."');
  await takeScreenshot('04 - Receive Page');
}

async function overviewPage() {
  const { page } = storage.getStore();
  await clickTabbar('Overview');
  await page.click('text=Overview');
  await page.waitForSelector('text=Kaladin Stormblessed');
  await page.getByText('5: Gate@Dirt Road').scrollIntoViewIfNeeded();
  await page.hover('text=__:__ >> nth=40');
  await page.waitForTimeout(2000);
  await takeScreenshot('05 - Overview Page');
}

async function takeScreenshot(name) {
  const store = storage.getStore();
  await store.page.screenshot({ path: `${OUTPUT_DIR}/${store.device.name}/${name}.png` });
  console.log(`Screenshot: \x1b${store.device.color}${store.device.name}/${name}.png\x1b[0m`);
}

async function clickTabbar(locator) {
  /** @type { import('playwright').Page } */
  const page = storage.getStore().page;
  await page.click(`.v-bottom-navigation .v-btn:has-text("${locator}")`);
}

async function clickSettings(locator) {
  /** @type { import('playwright').Page } */
  const page = storage.getStore().page;
  await page.click('.title-bar .v-icon:has-text("settings")');
  await page.click(locator);
  await page.click('.title-bar .v-icon:has-text("settings")');
  await page.waitForSelector('.v-menu__content', { state: 'hidden' });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
async function removeElements(selector) {
  const { page } = storage.getStore();
  await page.locator(selector).first().waitFor();
  await page.evaluate((sel) => document.querySelectorAll(sel).forEach(el => el.remove()), selector);
}


async function main() {
  try {
    if (process.env.CI) {
      console.log("Starting web server...");
      exec('npm run preview');
      await sleep(5000);
    }
    console.log("Generating screenshots...");
    await runTests();
    process.exit(0);
  } catch(err) {
    console.error(err);
    process.exit(1);
  }
}

main();
