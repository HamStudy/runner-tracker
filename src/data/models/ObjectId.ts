/*
*
* Copyright (c) 2011-2014- Justin Dearing (zippy1981@gmail.com)
* Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
* and GPL (http://www.opensource.org/licenses/gpl-license.php) version 2 licenses.
* This software is not distributed under version 3 or later of the GPL.
*
* Version 1.0.2
*
*/

import {random} from '@/lib/randomNum';


/**
 * Javascript class that mimics how WCF serializes a object of type MongoDB.Bson.ObjectId
 * and converts between that format and the standard 24 character representation.
 */

let hasDocument = typeof document !== "undefined";
let increment = random(16777216, 3);
let pid = random(65536, 2);
let machine = random(16777216, 3);
function setMachineCookie() {
    let cookieList = hasDocument ? document.cookie.split('; ') : [];
    (cookieList||[]).some(entry => {
        let cookie = entry.split('=');
        let cookieMachineId = parseInt(cookie[1], 10);
        if (cookie[0] === 'mongoMachineId' && cookieMachineId && cookieMachineId >= 0 && cookieMachineId <= 16777215) {
            machine = cookieMachineId;
            return true;
        }
    });
    if (hasDocument) {
      document.cookie = `mongoMachineId=${machine};expires=Tue, 19 Jan 2038 05:00:00 GMT;path=/`;
    }
}
if (typeof (localStorage) !== 'undefined') {
    try {
        let mongoMachineId = parseInt(localStorage['mongoMachineId'], 10);
        if (mongoMachineId >= 0 && mongoMachineId <= 16777215) {
            machine = Math.floor(localStorage['mongoMachineId']);
        }
        // Just always stick the value in.
        localStorage['mongoMachineId'] = machine;
    } catch (e) {
        setMachineCookie();
    }
} else {
    setMachineCookie();
}

export interface IMongoObjectId {
    timestamp: number;
    machine: number;
    pid: number;
    increment: number;
}
export default class ObjectId implements IMongoObjectId {
    timestamp: number;
    machine: number;
    pid: number;
    increment: number;

    constructor(objId?: IMongoObjectId);
    constructor(objId: string);
    constructor(timestamp: number, inMachine: number, inPid: number, inIncrement: number);
    constructor(idOrTs?: string|number|IMongoObjectId, inMachine?: number, inPid?: number, inIncrement?: number) {
        if (typeof (idOrTs) === 'object') {
            let objId = idOrTs as IMongoObjectId;
            this.timestamp = objId.timestamp;
            this.machine = objId.machine;
            this.pid = objId.pid;
            this.increment = objId.increment;
        } else if (typeof idOrTs === 'string' && idOrTs.length === 24) {
            this.timestamp = Number('0x' + idOrTs.substring(0, 8));
            this.machine = Number('0x' + idOrTs.substring(8, 14));
            this.pid = Number('0x' + idOrTs.substring(14, 18));
            this.increment = Number('0x' + idOrTs.substring(18, 24));
        } else if (arguments.length === 4 && arguments[0] != null) {
            this.timestamp = idOrTs as number;
            this.machine = inMachine!;
            this.pid = inPid!;
            this.increment = inIncrement!;
        } else {
            this.timestamp = Math.floor(new Date().valueOf() / 1000);
            this.machine = machine;
            this.pid = pid;
            this.increment = increment++;
            if (increment > 0xffffff) {
                increment = 0;
            }
        }
    }

    getDate() {
        return new Date(this.timestamp * 1000);
    }
    toArray() {
        let strOid = this.toString();
        let array: number[] = [];
        for(let i = 0; i < 12; i++) {
            array[i] = parseInt(strOid.slice(i*2, i*2+2), 16);
        }
        return array;
    }
    /**
     * Turns a WCF representation of a BSON ObjectId into a 24 character string representation.
     */
    toString() {
        if (this.timestamp === undefined
            || this.machine === undefined
            || this.pid === undefined
            || this.increment === undefined) {
            return 'Invalid ObjectId';
        }

        let timestamp = this.timestamp.toString(16);
        let curMach = this.machine.toString(16);
        let curPid = this.pid.toString(16);
        let curInc = this.increment.toString(16);
        return '00000000'.substring(0, 8 - timestamp.length) + timestamp +
               '000000'.substring(0, 6 - curMach.length) + curMach +
               '0000'.substring(0, 4 - curPid.length) + curPid +
               '000000'.substring(0, 6 - curInc.length) + curInc;
    }
}
