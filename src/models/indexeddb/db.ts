// import { openDB, DBSchema } from 'idb';
// import {txDbName, rxDbName, ITransmissionDoc} from '../base/transmission';
// import {dbName as EntryStoreName, IEntryDoc} from '../base/entry';
// import {dbName as EventStoreName, IEventDoc} from '../base/event';
// import {dbName as RaceStoreName, IRaceDoc} from '../base/race';
// import {dbName as StationStoreName, IStationDoc} from '../base/station';
// import {dbName as ParticipantStoreName, IParticipantDoc} from '../base/participant';


// export interface RunnerTrackerDB extends DBSchema {
//   [rxDbName]: {
//     key: string;
//     value: ITransmissionDoc;
//     indexes: {
//       'eventId': 'eventId';
//       'stationId': 'stationId';
//       'event-hash': ['eventId','hash'],
//     },
//   };
//   [txDbName]: {
//     key: string;
//     value: ITransmissionDoc;
//     indexes: {
//       'eventId': 'eventId';
//       'stationId': 'stationId';
//       'event-hash': ['eventId','hash'],
//       'event-timestamp': ['eventId','timestamp'];
//     },
//   };
//   [EventStoreName]: {
//     key: string;
//     value: IEventDoc;
//   };
//   [RaceStoreName]: {
//     key: string;
//     value: IRaceDoc;
//     indexes: {
//       'eventId': 'eventId';
//       'event-name': ['eventId', 'name'],
//       'id-event': ['id', 'eventId'],
//     };
//   };
//   [StationStoreName]: {
//     key: string;
//     value: IStationDoc;
//     indexes: {
//       'eventId': 'eventId';
//       'id-event': ['id', 'eventId'],
//       'event-stationNum': ['eventId', 'stationNumber'];
//       'race-stationNum': ['raceId', 'stationNumber'];
//     };
//   };
//   [ParticipantStoreName]: {
//     key: string;
//     value: IParticipantDoc;
//     indexes: {
//       'eventId': 'eventId';
//       'event-bibNum': ['eventId', 'bibNumber'];
//     };
//   };
//   [EntryStoreName]: {
//     key: string;
//     value: IEntryDoc;
//     indexes: {
//       'eventId': 'eventId',
//       'stationId': 'stationId',
//       'participantId': 'participantId',
//       'stationId-participantId': ['stationId', 'participantId'];
//       'stationId-eventId': ['stationId', 'eventId'];
//       'stationId-modified': ['stationId', 'modified'];
//     };
//   };
// }

// const dbProm = (async () => {
//   // check for support
//   if (!('indexedDB' in window)) {
//     throw new Error('This browser doesn\'t support IndexedDB');
//   }

//   const db = await openDB<RunnerTrackerDB>('runner-tracker', 1, {
//     upgrade(udb, oldVersion, newVersion, transaction) {
//       if (!udb.objectStoreNames.contains(rxDbName)) {
//         const os = udb.createObjectStore(rxDbName, {keyPath: 'hash'});
//         os.createIndex('eventId', 'eventId');
//         os.createIndex('stationId', 'stationId');
//         os.createIndex('event-hash', ['eventId','hash']);
//       }
//       if (!udb.objectStoreNames.contains(txDbName)) {
//         const os = udb.createObjectStore(txDbName, {keyPath: 'hash'});
//         os.createIndex('eventId', 'eventId');
//         os.createIndex('stationId', 'stationId');
//         os.createIndex('event-timestamp', ['eventId','timestamp']);
//         os.createIndex('event-hash', ['eventId','hash']);
//       }
//       if (!udb.objectStoreNames.contains(RaceStoreName)) {
//         const os = udb.createObjectStore(RaceStoreName, {keyPath: 'id'});
//         os.createIndex('eventId', 'eventId');
//         os.createIndex('event-name', ['eventId', 'name'], {unique: true});
//         os.createIndex('id-event', ['id', 'eventId'], {unique: true});
//       }
//       if (!udb.objectStoreNames.contains(StationStoreName)) {
//         const os = udb.createObjectStore(StationStoreName, {keyPath: 'id'});
//         os.createIndex('eventId', 'eventId');
//         os.createIndex('id-event', ['id', 'eventId'], {unique: true});
//         os.createIndex('event-stationNum', ['eventId', 'stationNumber'], {unique: false});
//         os.createIndex('race-stationNum', ['raceId', 'stationNumber'], {unique: true});
//       }
//       if (!udb.objectStoreNames.contains(EventStoreName)) {
//         const os = udb.createObjectStore(EventStoreName, {keyPath: 'id'});
//       }
//       if (!udb.objectStoreNames.contains(ParticipantStoreName)) {
//         const os = udb.createObjectStore(ParticipantStoreName, {keyPath: 'id'});
//         os.createIndex('eventId', 'eventId');
//         os.createIndex('event-bibNum', ['eventId', 'bibNumber']);
//       }
//       if (!udb.objectStoreNames.contains(EntryStoreName)) {
//         const os = udb.createObjectStore(EntryStoreName, {keyPath: 'id'});
//         os.createIndex('eventId', 'eventId');
//         os.createIndex('stationId', 'stationId');
//         os.createIndex('participantId', 'participantId');
//         os.createIndex('stationId-participantId', ['stationId', 'participantId'], {unique: true});
//         os.createIndex('stationId-eventId', ['stationId', 'eventId']);
//         os.createIndex('stationId-modified', ['stationId', 'modified']);
//       }
//     },
//   });
//   return db;
// })();

// export { dbProm };

// if (isDev) {
//   (async () => {
//     (window as any).db = await dbProm;
//   })();
// }
