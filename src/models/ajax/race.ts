import { AxiosError } from 'axios';
import { axios } from './config';
import Base, { IRaceDoc } from '../base/race';


export class Race extends Base {
  static async getRace({id, eventId, name}: {eventId: string; id?: string; name?: string}): Promise<Race | null> {
    if (!id && !name) { return null; }
    let doc: IRaceDoc | null = null;
    try {
      const resp = await axios.get(`/api/v1/events/${eventId}/races/${id || name}`);
      doc = resp.data;
    } catch (e) {
      if ((e as AxiosError).response?.status !== 404) {
        console.error(e); // tslint:disable-line no-console
      }
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getRaces(eventId: string): Promise<Race[]> {
    const docs: Race[] = [];
    try {
      const resp = await axios.get(`/api/v1/events/${eventId}/races`);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e: any) {
      if (e.response?.status !== 404) {
        console.error(e); // tslint:disable-line no-console
      }
    }
    return docs;
  }
  static async delete(race: Race) {
    await axios.delete(`/api/v1/events/${race.eventId}/races/${race.id}`);
  }

  async save() {
    try {
      if (this.id) {
        await axios.put<IRaceDoc>(`/api/v1/events/${this.eventId}/races/${this.id}`, dbConverter.toDB(this));
      } else {
        const resp = await axios.post<IRaceDoc>(`/api/v1/events/${this.eventId}/races`, dbConverter.toDB(this));
        this.id = resp.data.id;
      }
    } catch (err) {
      const response = (err as AxiosError).response;
      if (response?.status === 409 && typeof response.data === 'object') {
        console.error(response.data.message = `Error Race ${this.name} already exists.`); // tslint:disable-line no-console
      } else {
        throw err;
      }
    }
  }
}
export default Race;

// DB data converter
const dbConverter = {
  toDB(x: Race) {
    const doc: IRaceDoc = {
      id: x.id,
      eventId: x.eventId,
      name: x.name,
      stations: x.stations,
    };
    return doc;
  },
  fromDB(doc: IRaceDoc) {
    const id = doc.id;
    return new Race(doc.eventId, Object.assign({}, doc));
  },
};
