import { AxiosError } from 'axios';
import { axios } from './config';
import Base, { IEventDoc } from '../base/event';


export class Event extends Base {
  static async getEvent(idOrSlug: string): Promise<Event | null> {
    if (!idOrSlug) { return null; }
    let doc: IEventDoc | null = null;
    try {
      const resp = await axios.get(`/api/v1/events/${idOrSlug}`);
      doc = resp.data;
    } catch (e) {
      if ((e as AxiosError).response?.status !== 404) {
        console.error(e); // tslint:disable-line no-console
      }
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getEvents(): Promise<Event[]> {
    const docs: Event[] = [];
    try {
      const resp = await axios.get(`/api/v1/events`);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e) {
      console.error(e); // tslint:disable-line no-console
    }
    return docs;
  }
  static async delete(event: Event) {
    await axios.delete(`/api/v1/events/${event.id}`);
  }

  async save() {
    try {
      if (this.id) {
        await axios.put<IEventDoc>(`/api/v1/events/${this.id}`, dbConverter.toDB(this));
      } else {
        const resp = await axios.post<IEventDoc>(`/api/v1/events`, dbConverter.toDB(this));
        this.id = resp.data.id;
      }
    } catch (err) {
      const response = (err as AxiosError).response;
      if (response?.status === 409 && typeof response.data === 'object') {
        console.error(`Error Event ${this.slug ? this.slug : this.id} already exists.`); // tslint:disable-line no-console
      } else {
        throw err;
      }
    }
  }
}
export default Event;

// DB data converter
const dbConverter = {
  toDB(x: Event) {
    const doc: IEventDoc = {
      id: x.id,
      name: x.name,
      startDate: x.startDate,
      endDate: x.endDate,
      slug: x.slug,
    };
    return doc;
  },
  fromDB(doc: IEventDoc) {
    const startDate = doc.startDate ? new Date(doc.startDate) : (doc as any).date ? new Date((doc as any).date) : (void 0);
    const endDate = doc.endDate ? new Date(doc.endDate) : (void 0);
    const id = doc.id;
    const slug = doc.slug;
    return new Event({id, name: doc.name, startDate, endDate, slug});
  },
};
