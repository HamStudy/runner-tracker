import { type AxiosError } from 'axios';
import { axios } from './config';
import Base, { type IStationDoc } from '../base/station';


export class Station extends Base {
  // This is used to verify that the station id we have belongs to this event, if not it'll be null
  static async getStation(obj: {eventId: string; stationId?: string}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation(obj: {eventId: string; raceId?: string; stationNumber: number}): Promise<Station | null>; // tslint:disable-line unified-signatures
  static async getStation({eventId, raceId, stationId, stationNumber}: Partial<{eventId: string; raceId: string; stationId: string; stationNumber: number}>): Promise<Station | null> {
    let doc: IStationDoc | undefined;
    try {
      let url = '';
      if (stationId) {
        url = `/api/v1/station/${stationId}`;
        if (eventId) {
          url += `?event=${eventId}`;
        }
      } else if (eventId && typeof stationNumber === 'number') {
        url = `/api/v1/events/${eventId}/stations/${stationNumber}`;
        if (raceId) {
          url += `?raceId=${raceId}`;
        }
      }
      if (!url) { return null; }
      const resp = await axios.get(url);
      doc = resp.data;
    } catch (e) {
      if ((e as AxiosError).response?.status !== 404) {
        console.error(e); // tslint:disable-line no-console
      }
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }

  static async getStations(eventId: string, raceId?: string | undefined) {
    const docs: Station[] = [];
    try {
      let url = `/api/v1/events/${eventId}/stations`;
      if (raceId) {
        url += `?raceId=${raceId}`;
      }
      const resp = await axios.get(url);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e) {
      console.error(e); // tslint:disable-line no-console
    }
    return docs;
  }

  static async delete(station: Station) {
    await axios.delete(`/api/v1/station/${station.id}`);
  }

  async save() {
    try {
      if (this.id) {
        await axios.put<Station>(`/api/v1/station/${this.id}`, dbConverter.toDB(this));
      } else {
        const resp = await axios.post<Station>(`/api/v1/station`, dbConverter.toDB(this));
        this.id = resp.data.id;
      }
    } catch (err) {
      const response = (err as AxiosError).response;
      if (response?.status === 409 && typeof response.data === 'object') {
        console.error(response.data.message = `Error Station ${this.stationNumber}: ${this.name} already exists.`); // tslint:disable-line no-console
      } else {
        throw err;
      }
    }
  }
}
export default Station;

// DB data converter
const dbConverter = {
  toDB(x: Station) {
    return {
      id: x.id,
      eventId: x.eventId,
      name: x.name,
      distance: x.distance,
      raceId: x.raceId,
      stationNumber: x.stationNumber,
      stationNumberDisplayed: x.stationNumberDisplayed,
    };
  },
  fromDB(doc: IStationDoc) {
    const raceEventId = String(doc.eventId);
    return new Station(raceEventId, Object.assign({}, doc));
  },
};
