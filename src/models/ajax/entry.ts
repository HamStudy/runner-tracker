import { AxiosError } from 'axios';
import { axios } from './config';
import Base, { EntryQuery, IEntryDoc } from '../base/entry';


export class Entry extends Base {
  static async getEntry(id: string): Promise<Entry | null>;
  static async getEntry(stationId: string, participantId: string): Promise<Entry | null>; // tslint:disable-line unified-signatures
  static async getEntry(id: string, participantId?: string): Promise<Entry | null> {
    let doc: IEntryDoc | undefined;
    let url = `/api/v1/entry`;
    if (!participantId) {
      url += `/${id}`;
    } else {
      url += `?participantId=${participantId}&stationId=${id}`;
    }
    try {
      const resp = await axios.get(url);
      doc = resp.data;
    } catch (e) {
      if ((e as AxiosError).response?.status !== 404) {
        console.error(e); // tslint:disable-line no-console
      }
    }
    return doc ? dbConverter.fromDB(doc) : null;
  }
  static async getEntrys({participantId, stationId, eventId, sinceDate}: EntryQuery) {
    const docs: Entry[] = [];
    let url = '';
    try {
      if (participantId) {
        url += `/api/v1/participant/${participantId}/entries`;
      } else if (stationId) {
        url += `/api/v1/station/${stationId}/entries`;
        if (sinceDate) {
          url += `?since${sinceDate.getTime()}`;
        }
      } else if (eventId) {
        url += `/api/v1/events/${eventId}/entries`;
      }
      const resp = await axios.get(url);
      for (const doc of resp.data) {
        docs.push(dbConverter.fromDB(doc));
      }
    } catch (e) {
      console.error(e); // tslint:disable-line no-console
    }
    return docs;
  }

  static async delete(entry: Entry) {
    await axios.delete(`/api/v1/entry/${entry.id}`);
  }

  async save() {
    try {
      if (this.id) {
        await axios.put<Entry>(`/api/v1/entry/${this.id}`, dbConverter.toDB(this));
      } else {
        const resp = await axios.post<Entry>(`/api/v1/entry`, dbConverter.toDB(this));
        this.id = resp.data.id;
      }
    } catch (err) {
      const response = (err as AxiosError).response;
      if (response?.status === 409 && typeof response.data === 'object') {
        console.error(`Error entry for participant ${this.participantId} and station ${this.stationId} already exists.`); // tslint:disable-line no-console
      } else {
        throw err;
      }
    }
  }
}
export default Entry;

// DB data converter
const dbConverter = {
  toDB(x: Entry) {
    return x;
  },
  fromDB(doc: IEntryDoc) {
    const id = doc.id;
    const timeIn = doc.timeIn && new Date(doc.timeIn as any);
    const timeOut = doc.timeOut && new Date(doc.timeOut as any);
    const modified = new Date((doc as any).updatedAt as any);
    const raceEventId = String(doc.eventId);
    const participantId = String(doc.participantId);
    const stationId = String(doc.stationId);
    return new Entry(raceEventId, participantId, stationId, {
      id,
      modified,
      timeIn,
      timeOut,
    });
  },
};
