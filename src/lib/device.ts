import { App } from '@capacitor/app';

export const info = App.getInfo();

export const isDev = process.env.NODE_ENV === 'development';
