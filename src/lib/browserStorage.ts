export default class BrowserStorage {
  addListener: any;
  removeListener: any;
  constructor(private storageEngine: Storage) {
  }
  async clear(): Promise<void> {
    this.storageEngine.clear();
  }
  async get(options: { key: string }): Promise<{ value: string; }> {
    return { value: this.storageEngine.getItem(options.key) || '' };
  }
  async keys(): Promise<{ keys: string[]; }> {
    return { keys: Object.keys(this.storageEngine) };
  }
  async remove(options: { key: string }): Promise<void> {
    this.storageEngine.removeItem(options.key);
  }
  async set(options: { key: string, value: string }): Promise<void> {
    // if (data === void 0) { return this.remove(storageKey); }
    this.storageEngine.setItem(options.key, options.value);
  }
}
