
import {
  initialize,
  MT63Client,
  wasmModule,
} from '@hamstudy/mt63-wasm';


export function loadWasm(bin: Uint8Array) {
  return initialize(mod => {
    mod.wasmBinary = bin;
    return mod;
  });
}

export {MT63Client, wasmModule};
