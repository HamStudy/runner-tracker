// tslint:disable no-console
// import { MT63Client, readyDfd as wasmReady } from '@/lib/mt63';


(<any>globalThis).importScripts = (...args: any[]) => {
  console.warn("ImportScripts not supported in worklets", args);
};
(<any>globalThis).location = {
  href: '',
};

// import { Resampler } from '../resampler';
import { MT63Client, loadWasm, wasmModule } from '@/lib/mt63/workletShim';
import { downSample } from './downsample';
if (typeof window !== 'undefined') {
  (window as any).MT63Client = MT63Client;
}


interface AudioWorkletProcessor {
  readonly port: MessagePort;
  process(
    inputs: Float32Array[][],
    outputs: Float32Array[][],
    parameters: Record<string, Float32Array>,
  ): boolean;
}

declare const AudioWorkletProcessor: {
  prototype: AudioWorkletProcessor;
  new (options?: AudioWorkletNodeOptions): AudioWorkletProcessor;
};

declare function registerProcessor(
  name: string,
  processorCtor: (new (
    options?: AudioWorkletNodeOptions,
  ) => AudioWorkletProcessor),
): void;
declare const sampleRate: number;

declare namespace globalThis {
  let wasmLoaded: boolean;
  let mtClient: MT63Client;
}

// This needs to be a multiple of 6 and ideally 128
const chunkSize = 6 * 128 * 3;
const resampledSize = Math.ceil(chunkSize*(8000/sampleRate));
class MT63AudioProcessor extends AudioWorkletProcessor {
  mtClient?: import('@/lib/mt63').MT63Client;

  active = true;
  loc = 0;
  buffer: Float32Array | null = new Float32Array(chunkSize);
  bufferPtr = -1;
  resampleBuffer: Float32Array | null = null;
  resampleBufferPtr = -1;
  wasmLoading = false;

  workletBuffer: Float32Array | null = null;
  workletPtr = -1;

  constructor() {
    super();

    const pThis = this;
    this.port.onmessage = function (ev) {
      pThis.onMessage(this, ev);
    };
    if (globalThis.wasmLoaded) {
      this.loadWasm();
    } else {
      this.port.postMessage({req: 'startup'});
    }
  }

  async loadWasm(binFile?: Uint8Array) {
    try {
      if (globalThis.wasmLoaded) {
        this.mtClient = globalThis.mtClient;
        return;
      } else if (!binFile) {
        throw new Error("Can't load wasm - no binary provided");
      }
      this.wasmLoading = true;
      await loadWasm(binFile);
      this.wasmLoading = false;
      globalThis.mtClient = this.mtClient = new MT63Client();
      const oldBuff = this.buffer;
      const newBufPtr = wasmModule.mod._malloc((chunkSize + 10) * 4);
      this.workletBuffer = wasmModule.mod.HEAPF32.subarray(newBufPtr/4, newBufPtr/4 + chunkSize) as Float32Array;
      this.workletPtr = newBufPtr;
      console.log("Allocated memory at ", newBufPtr);
      if (oldBuff) {
        this.workletBuffer.set(oldBuff); // Copy from the old buffer
      }
      this.buffer = null;
      globalThis.wasmLoaded = true;
    } catch (err) {
      console.warn("Error loading wasm: ", err);
    }
  }

  onMessage(port: MessagePort, ev: MessageEvent<any>) {
    if (ev.data.binary) {
      this.loadWasm(ev.data.binary);
    } else if (ev.data.req === 'shutdown') {
      this.active = false;
    }
  }

  process(inputs: Float32Array[][], outputs: Float32Array[][], parameters: Record<string, Float32Array>) {
    const inp = inputs[0][0]; // first input, first channel

    let {loc} = this;
    const remaining = chunkSize - loc;

    const bufToUse = this.workletBuffer ? this.workletBuffer : this.buffer;
    const ptrToUse = this.workletBuffer ? this.workletPtr : this.bufferPtr;
    if (!bufToUse) { return this.active; }
    if (inp.length >= remaining) {
      // we need to split the array;

      bufToUse.set(inp.subarray(0, remaining), loc);
      // const maxVal =  this.buffer.reduce((memo, cur) => Math.max(memo, cur), 0);
      // console.log("Sending data across the great divide...", maxVal);
      this.processAudio(bufToUse, ptrToUse, chunkSize);

      // Shouldn't actually need to make a new array, we can keep using the old one
      // this.buffer = new Float32Array(chunkSize);
      const remnantSize = inp.length - remaining;
      bufToUse.set(inp.subarray(remaining, inp.length), 0);
      loc = remnantSize;
    } else {
      // append to the array
      bufToUse.set(inp, loc);
      loc += inp.length;
    }
    this.loc = loc;

    return this.active;
  }

  lastWasWorklet = null as boolean | null;
  reportWork(isWorklet: boolean) {
    if (this.lastWasWorklet !== isWorklet) {
      console.log("Processing in the worklet? ", isWorklet);
    }
    this.lastWasWorklet = isWorklet;
  }
  processAudio(floatArr: Float32Array, floatPtr: number, len: number) {
    const bufToUse = this.workletBuffer ? this.workletBuffer : this.buffer;
    const ptrToUse = this.workletBuffer ? this.workletPtr : this.bufferPtr;
    // console.log("Processing with", ptrToUse, len);
    if (this.mtClient && wasmModule && this.workletBuffer) {
      this.reportWork(true);

      const res = wasmModule._processResampleMT63Rx(ptrToUse, sampleRate, len);
      if (res?.length) {
        this.port.postMessage({decoded: res});
      }
    } else {
      this.reportWork(false);
      // let ReSamp = new Resampler(sampleRate, 8000, 1, floatArr);
      if (!this.resampleBuffer) {
        this.resampleBuffer = new Float32Array(resampledSize);
      }
      const size = downSample(floatArr, len, sampleRate, 8000, this.resampleBuffer);
      // len = ReSamp.resampler(len);
      floatArr = this.resampleBuffer.subarray(0, size);
      this.port.postMessage({audioBuffer: floatArr, sampleRate: 8000});
    }
  }

}

registerProcessor('mt63-processor', MT63AudioProcessor);
