
/* eslint-disable @typescript-eslint/ban-types */

export type UnknownObject = Record<string, unknown>;

export type Callable<R = any> = (...args: any) => R;
export type UnpromiseReturnType<T extends Callable> =
  T extends Callable<Promise<infer U>> ? U :
  T extends Callable<infer U> ? U : unknown; // tslint:disable-line:no-shadowed-variable
export type NotUndefined<T> = T extends undefined ? never : T;

export function keys<T extends object>(obj: T): Array<keyof T> {
  return Object.keys(obj) as Array<keyof T>;
}

export function pick<T extends object, K extends keyof T>(objIn: T, fields: K[] | Set<K> | readonly K[]): Pick<T, K>;
export function pick<T extends object, K extends keyof T>(objIn: T, ...fields: K[]): Pick<T, K>;
export function pick<T extends object, K extends keyof T>(objIn: T, ...args: any[]): Pick<T, K> {
  const fields: K[] | Set<K> | readonly K[] = Array.isArray(args[0]) ? args[0] : args;
  const objOut: Pick<T, K> = {} as any;

  for (const f of fields) {
    objOut[f] = objIn[f];
  }
  return objOut;
}

export function basename(path: string) {
  const idx = path.lastIndexOf('.');
  return (idx > -1) ? path.substring(0, idx) : path;
}

export function asNumber(strOrNum: string | number) {
  if (typeof strOrNum === 'string') {
    return parseInt(strOrNum, 10);
  } else {
    return strOrNum;
  }
}

export async function asyncArrayFrom<T, R>(gen: AsyncIterable<T>, xlate: (v: T) => R): Promise<R[]> {
  const out: R[] = [];
  for await(const x of gen) {
    out.push(xlate(x));
  }
  return out;
}

export function forceObj<T extends object>(obj: T | null | undefined): T {
  return obj ?? {} as T;
}
export function forceArr<T extends unknown[] | readonly unknown[]>(arr: T | null | undefined): T {
  return arr || (<unknown[]> []) as T;
}

