import { Preferences } from '@capacitor/preferences';
import type { PreferencesPlugin } from '@capacitor/preferences';

export class KvStorageInstance<T> {
  constructor(private storage: PreferencesPlugin = Preferences, private prefix: string) {
  }

  /**
   * Get the value associated with the given key.
   * @param {any} key the key to identify this value
   * @returns {Promise} Returns a promise with the value of the given key
   */
  async get(key: string): Promise<T | null> {
    const { value } = await this.storage.get({key: this.getRawKey(key)});
    if (value === null) { return null; }
    return JSON.parse(value);
  }
  /**
   * Set the value for the given key.
   * @param {any} key the key to identify this value
   * @param {any} value the value for this key
   * @returns {Promise} Returns a promise that resolves when the key and value are set
   */
  set(key: string, value: T): Promise<void> {
    return this.storage.set({key: this.getRawKey(key), value: JSON.stringify(value)});
  }
  /**
   * Remove the given key/value from the storage
   * @param {any} key the key to be removed
   * @returns {Promise} Returns a promise that resolves when the key and value are removed from storage
   */
  remove(key: string): Promise<void> {
    return this.storage.remove({key: this.getRawKey(key)});
  }

  // Get all keys with a given prefix
  async list(): Promise<T[]> {
    let { keys } = await this.storage.keys();

    keys = keys.filter((k) => k.startsWith(this.prefix));

    return await Promise.all(keys.map((k) => this._rawGet(k)).filter((k) => k !== null)) as T[];
  }

  async keys(): Promise<string[]> {
    let { keys } = await this.storage.keys();

    keys = keys
      .filter((k) => k.startsWith(this.prefix))
      .map((k) => k.substring(this.prefix.length))
    ;

    return keys;
  }

  private getRawKey(key: string): string {
    return `${this.prefix}${key}`;
  }
  private async _rawGet(rawKey: string): Promise<T | null> {
    const { value } = await this.storage.get({ key: rawKey });
    if (value === null) { return null; }
    return JSON.parse(value);
  }
}
